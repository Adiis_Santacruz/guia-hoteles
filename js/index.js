
$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    /*para colocar el intervalo de transicion del carrusel*/
    $('.carousel').carousel({
        interval: 2000
    });
    $('#contact').on('show.bs.modal', function (e) {
        console.log('el moal se esta mostrando');
        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-primary');
        $('#contactoBtn').prop('disabled', true);


    });
    $('#contact').on('shown.bs.modal', function (e) {
        console.log('el modal se mostró');
    });
    $('#contact').on('hide.bs.modal', function (e) {
        console.log('el modal se esta ocultando');
        $('#contactoBtn').removeClass('btn-primary');
        $('#contactoBtn').addClass('btn-outline-success');
        $('#contactoBtn').prop('disabled', false);
    });
    $('#contact').on('hiden.bs.modal', function (e) {
       
    });
});
